
from __future__ import absolute_import, print_function

import os
import os.path
from io import open
import lxml.etree
import subprocess

EMULAB_EXTNS = "http://www.protogeni.net/resources/rspec/ext/emulab/1"
TMCC = "/usr/local/etc/emulab/tmcc.bin"

geni_key = None
manifest = None

#
# XXX: cryptography lacks support for smime decrypt --
# https://github.com/pyca/cryptography/issues/6263 .
# At least we don't have to write the ciphertext to disk,
# not that it matters since all this data is available via
# tmcd.
#
def decrypt(ciphertext):
    global geni_key
    if not geni_key:
        proc = subprocess.run([TMCC,"geni_key"], capture_output=True)
        geni_key = proc.stdout.decode().lstrip("\0").rstrip("\n")
    geni_key_path = "/tmp/geni.key"
    with open(geni_key_path,'wb') as f:
        f.write(geni_key.encode("utf-8"))
    command = [ "/usr/bin/openssl", "smime", "-decrypt", "-inform", "PEM",
                "-inkey", geni_key_path, "-in", "-" ]
    proc = subprocess.run(command, input=ciphertext.encode(), capture_output=True)
    os.unlink(geni_key_path)
    return proc.stdout.decode()

def convert(p,v):
    if v in [ "True","true",True ]:
        return True
    elif v in  [ "False","false",False ]:
        return False
    #elif v == None:
    #    return ""
    return v

def parse_manifest():
    global manifest

    if not manifest:
        proc = subprocess.run([TMCC,"geni_manifest"], capture_output=True)
        if proc.returncode or not proc.stdout:
            return dict()
        manifest = proc.stdout.decode().lstrip("\0")
    
    root = lxml.etree.fromstring(manifest)

    manifest_dict = dict()

    # Find nodes
    nodes = dict()
    for elm in root.getchildren():
        if not elm.tag.endswith("}node"):
            continue
        (host,ip) = (None,None)
        for elm2 in elm.getchildren():
            if elm2.tag.endswith("}host"):
                (host,ip) = (elm2.get("name",None),elm2.get("ipv4",None))
                break
        nodes[elm.get("client_id")] = dict(hostname=host,ip=ip)
    manifest_dict["nodes"] = nodes
    manifest_dict["nodenames"] = list(nodes)

    # Find parameters
    parameters = dict()
    for elm in root.getchildren():
        if elm.tag.endswith("}data_set"):
            for elm2 in elm.getchildren():
                if elm2.tag.endswith("}data_item"):
                    p = elm2.get("name")
                    v = convert(p,elm2.text)
                    parameters[p] = v
        if elm.tag.endswith("}data_item"):
            p = elm.get("name")
            parameters[p] = convert(p,elm.text)
    manifest_dict["parameters"] = parameters

    # Find any passwords for decrypt
    passwords = dict()
    for elm in root.getchildren():
        if not elm.tag.endswith("}password"):
            continue
        passwords[elm.get("name")] = decrypt(elm.text)
    manifest_dict["passwords"] = passwords

    # RDZ specific info.
    rdzinfo = dict()
    for elm in root.getchildren():
        if elm.tag.endswith("}rdz-request"):
            rdzinfo = elm.attrib
            break
        pass
    manifest_dict["rdzinfo"] = rdzinfo

    # Spectrum.
    spectrum = dict()
    for elm in root.getchildren():
        if elm.tag.endswith("}spectrum"):
            attributes = elm.attrib
            if "rdz_grant_id" in attributes:
                spectrum[attributes["rdz_grant_id"]] = attributes
                pass
            pass
        pass
    manifest_dict["spectrum"] = spectrum

    return manifest_dict

if __name__ == "__main__":
    print(str(parse_manifest()))
