#!/usr/bin/perl -w

use strict;
use Carp;
use English;
use Data::Dumper;
use Getopt::Std;
use File::Temp qw(tempfile);
use File::Basename;
use POSIX qw(isatty setsid);
use XML::Simple;

# Drag in path stuff so we can find emulab stuff.
BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

#
# Transmissions to tickle the RDZ
#
sub usage()
{
    print STDOUT "Usage: rdztest [-i] [-g gain] <goodfreq> [<badfreq>]\n";
    exit(-1);
}
my $optlist     = "ig:";
my $noinstall   = 0;

my $TIMEOUT     = 60; # seconds.
my $LOGFILE     = "/var/tmp/transmit.log";
my $READY       = "/etc/.txready";
my $TX          = "/usr/libexec/uhd/examples/tx_waveforms";
my $REPO        = "/local/repository";
my $INSTALL     = "$REPO/install.sh";
my $FREQ        = 3540;
my $GAIN        = 15;
my $RATE        = "2e6";
my $ANTENNA     = "TX/RX";
my $CHANNEL     = 0;

#
# HOME will not be defined until new images are built.
#
my $HOME = $ENV{"HOME"};
if (!defined($HOME)) {
    $HOME = "/users/geniuser";
    $ENV{"HOME"} = $HOME;
    $ENV{"USER"} = "geniuser";
}

# Protos
sub fatal($);

#
# Parse command arguments.
#
my %options = ();
if (! getopts($optlist, \%options)) {
    usage();
}
if (defined($options{"i"})) {
    $noinstall = 1;
}
if (defined($options{"g"})) {
    $GAIN = $options{"g"}; 
}
usage()
    if (@ARGV < 1);
my $goodfreq = shift(@ARGV);
if ($goodfreq < 10000) {
    $goodfreq *= 1000000;
}
my $badfreq;
if (@ARGV) {
    $badfreq = shift(@ARGV);
    if ($badfreq < 10000) {
	$badfreq *= 1000000;
    }
}
else {
    $badfreq = $goodfreq - 5000000;
}

#
# Save off our output when not interactive, so that we can send it
# someplace useful. 
#
if (! -t) {
    open(STDOUT, "> $LOGFILE") or
	die("opening $LOGFILE for STDOUT: $!");
    open(STDERR, ">> $LOGFILE") or
	die("opening $LOGFILE for STDERR: $!");
}

#
# Look for the narrowed frequency range in the manifest and use that.
#
system("geni-get manifest > /tmp/manifest.xml");
if ($?) {
    die("Could not get the manifest");
}
my %xmloptions = ('SuppressEmpty' => undef);
my $manifest = XMLin("/tmp/manifest.xml", %xmloptions);
if (!$manifest) {
    die("Could not parse the manifest");
}
if (exists($manifest->{'emulab:spectrum'})) {
    my $spectrum = $manifest->{'emulab:spectrum'};
    #print Dumper($spectrum);
    $goodfreq = $spectrum->{'frequency_low'};
    if ($goodfreq < 10000) {
	$goodfreq *= 1000000;
    }
    $badfreq  = $goodfreq - 5000000;
}

#
# Transmit for while on the good freq. 
#
my $command = "$TX --ant $ANTENNA --channel $CHANNEL --rate $RATE --gain $GAIN ";

my $childpid = fork();
if ($childpid) {
    my $timedout = 0;
    local $SIG{ALRM} = sub { kill("TERM", $childpid); $timedout = 1; };
    alarm $TIMEOUT;
    waitpid($childpid, 0);
    alarm 0;
    my $stat = $? >> 8;
    if (!$timedout) {
	print "Abnormal exit\n";
	exit(1);
    }
}
else {
    my $cmd = $command . "--freq $goodfreq";
    print "$cmd\n";
    # Child runs the transmitter
    exec($cmd);
}

#
# Transmit for while on the bad freq. 
#
sleep(2);
$childpid = fork();
if ($childpid) {
    my $timedout = 0;
    local $SIG{ALRM} = sub { kill("TERM", $childpid); $timedout = 1; };
    alarm $TIMEOUT * 2;
    waitpid($childpid, 0);
    alarm 0;
    my $stat = $? >> 8;
    if (!$timedout) {
	print "Abnormal exit\n";
	exit(1);
    }
}
else {
    my $cmd = $command . "--freq $badfreq";
    print "$cmd\n";
    # Child runs the transmitter
    exec($cmd);
}

#
# Transmit for while on the good freq. 
#
sleep(2);
$childpid = fork();
if ($childpid) {
    my $timedout = 0;
    local $SIG{ALRM} = sub { kill("TERM", $childpid); $timedout = 1; };
    alarm $TIMEOUT;
    waitpid($childpid, 0);
    alarm 0;
    my $stat = $? >> 8;
    if (!$timedout) {
	print "Abnormal exit\n";
	exit(1);
    }
}
else {
    my $cmd = $command . "--freq $goodfreq";
    print "$cmd\n";
    # Child runs the transmitter
    exec($cmd);
}

sub fatal($)
{
    my ($mesg) = $_[0];
    die("*** $0:\n".
	"    $mesg\n");
}

