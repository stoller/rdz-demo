#!/bin/python

import time
import uuid
import os
import sys
import signal
import traceback
import argparse
import logging
import asyncio
import shlex
import subprocess

from zmsclient.zmc.client import ZmsZmcClient
from zmsclient.zmc.v1.models import Subscription, EventFilter
from zmsclient.zmc.v1.models import Claim, Grant, Constraint
from zmsclient.common.subscription import ZmsSubscriptionCallback
import manifest

LOG = None

# Not sure where to pick these up
EVENT_TYPE_REPLACED    =     1
EVENT_TYPE_CREATED     =     2
EVENT_SOURCETYPE_ZMC   =     2
EVENT_CODE_GRANT       =     2006

# Only when a daemon
LOGFILE = "/local/logs/tx-wrapper.log"

# Command to run.
TXCMDTEST = "/bin/sleep 1000"
TXCMD = "/usr/libexec/uhd/examples/tx_waveforms --ant TX/RX --channel 0 " +\
    "--rate %r --gain 50 --freq %r "

#
# Initial load of the grant, chasing the replacement chain to the
# current (active) grant.
#
def loadGrant(zmc_client, grant_id):
    LOG.info("Loading initial grant %r", grant_id)
    grant = zmc_client.get_grant(grant_id, elaborate=True)
    if not grant:
        LOG.error("Could not get grant: %r " % (grant_id,))
        sys.exit(-1)
        pass

    while grant.replacement:
        grant_id = grant.replacement.new_grant_id
        LOG.info("Loading next grant %r", grant_id)
        grant = zmc_client.get_grant(grant_id, elaborate=True)
        if not grant:
            LOG.error("Could not get grant: %r " % (grant_id,))
            sys.exit(-1)
            pass
        pass

    return grant

#
# Subclass ZmsSubscriptionCallback, to handle grant events.
#
class ZMCSubscriptionCallback(ZmsSubscriptionCallback):
    def __init__(self, zmcclient, element_id,
                 grant_id=None, impotent=False, **kwargs):
        super(ZMCSubscriptionCallback, self).__init__(zmcclient, **kwargs)
        self.ZMC = zmcclient
        self.element_id = element_id
        self.impotent = impotent
        self.grant_id = grant_id
        self.runstate = "stopped"
        self.child = None
        self.grant = None

    async def start(self):
        # Need to load the grant we are watching
        self.grant = loadGrant(self.ZMC, self.grant_id)

        if self.grant.status == "active":
            self.startTransmitter()
            pass
        try:
            await self.run_callbacks()
        except Exception as ex:
            LOG.exception(ex)
            self.stopTransmitter()
            pass
        pass

    def stop(self):
        if self.child:
            self.stopTransmitter()
        pass

    def on_event(self, ws, evt, message):
        if evt.header.source_type != EVENT_SOURCETYPE_ZMC:
            LOG.error("on_event: unexpected source type: %r (%r)",
                      evt.header.source_type, message)
            return

        # Since we are subscribed to all ZMC events for this element,
        # there will be chatter we do not care about.
        if (evt.header.code != EVENT_CODE_GRANT):
            return

        try:
            self.handleEvent(evt.object_)
        except Exception as ex:
            LOG.exception(ex)
            pass
        pass

    def startTransmitter(self):
        min_freq = self.grant.constraints[0].constraint.min_freq
        max_freq = self.grant.constraints[0].constraint.max_freq
        freq = min_freq + ((max_freq - min_freq) / 2)
        rate = (max_freq - min_freq) / 4
        LOG.info("startTransmitter: %r,%r %r %r", min_freq, max_freq, freq, rate)
        
        command = TXCMD % (rate, freq)
        LOG.info(" %r", command)
        
        if self.child:
            LOG.info(" Stopping child that should not be running")
            self.child.terminate()
            self.child.wait(3)
            self.child = None
            pass
        fp = open("/tmp/tx.log", "w")
        self.child = subprocess.Popen(
            shlex.split(command), stdout=fp, stderr=subprocess.STDOUT)
        if self.child:
            # Give the process time to start.
            time.sleep(1)
            if self.child.poll() != None:
                LOG.error("Could not start TX process")
                self.child.wait()
                self.child = None
                return
            pass
        self.runstate = "running"
        pass

    def stopTransmitter(self):
        LOG.info("stopTransmitter")
        if not self.child:
            LOG.error(" Child is not running")
            return
        self.child.terminate()
        self.child.wait(1)
        self.child = None
        self.runstate = "stopped"
        pass

    def handleEvent(self, grant):
        old_grant_id = grant.id
        LOG.info("Grant event: %r - %r", old_grant_id, grant.status)

        #
        # Look for stop/start not related to grant replacement.
        #
        if not grant.replacement or not grant.replacement.new_grant_id:
            LOG.info("  Not a replacement grant")

            # Has to be an event for our current grant.
            if old_grant_id != self.grant.id:
                LOG.info("  Not our current grant, ignoring")
                return
 
            # Ignore the "replacing" state, has no meaning here.
            if grant.status == "replacing":
                LOG.info("  Ignoring replacing state")
                return

            if os.path.exists("/tmp/violate"):
                LOG.error("  Forcing a violation!")
                return
        
            if grant.status == "active" and self.runstate == "stopped":
                self.startTransmitter()
            elif grant.status != "active" and self.runstate == "running":
                self.stopTransmitter()
                pass
            return

        #
        # Grant replacement. Might be an extension or a change in spectrum.
        #
        new_grant_id = grant.replacement.new_grant_id
        grant = None
        LOG.info("Grant Replacement: %r -> %r", old_grant_id, new_grant_id)

        #
        # Since we loop for it to go active, ignore any events that
        # come in after that.
        #
        if new_grant_id == self.grant.id:
            LOG.info("  Our current grant, ignoring")
            return

        #
        # There can be a delay, so lets poll for a bit until it goes
        # active or is denied/deleted.
        #
        for x in range(30):
            time.sleep(2)
            grant = self.ZMC.get_grant(new_grant_id, elaborate=True)
            if grant.status in ["active", "denied", "deleted", "pending"]:
                break
            pass

        #
        # We get a new grant (replacement) even if the grant extension is
        # denied. We have to ignore that.
        #
        if grant.status != "active":
            LOG.error("  The replaced grant was not approved!")
            LOG.error("  Ignoring grant change")
            return

        LOG.info("  Expiration: %s", grant.expires_at)

        #
        # Lets see if the range changed. If no change, then just make
        # sure the transmitter is running. Otherwise, stop and restart
        # on new range.
        #
        old_min_freq = self.grant.constraints[0].constraint.min_freq
        old_max_freq = self.grant.constraints[0].constraint.max_freq
        new_min_freq = grant.constraints[0].constraint.min_freq
        new_max_freq = grant.constraints[0].constraint.max_freq
        LOG.info("  Old frequency min/max: %r/%r", old_min_freq, old_max_freq)
        LOG.info("  New frequency min/max: %r/%r", new_min_freq, new_max_freq)

        if os.path.exists("/tmp/violate"):
            LOG.error("  Forcing a violation!")
            return
        
        self.grant = grant
        if self.runstate == "stopped":
            self.startTransmitter()
            return

        if new_min_freq != old_min_freq or new_max_freq != old_max_freq:
            self.stopTransmitter()
            self.startTransmitter()
            return
        pass

# The hander has to be outside the async main.
def set_signal_handler(signum, task_to_cancel):
    def handler(_signum, _frame):
        asyncio.get_running_loop().call_soon_threadsafe(task_to_cancel.cancel)
    signal.signal(signum, handler)

def init_main():
    parser = argparse.ArgumentParser(
        prog="tx_wrapper",
        description="Handle grant change events for a transmitter")
    parser.add_argument(
        "-b", "--daemon", default=False, action="store_true",
        help="Daemonize")
    parser.add_argument(
        "-d", "--debug", default=0, action="count",
        help="Increase debug level: defaults to INFO; add once for zmsclient DEBUG; add twice to set the root logger level to DEBUG")
    parser.add_argument(
        "-n", "--impotent", default=False, action="store_true",
        help="Impotent: do not inject events")
    parser.add_argument(
        "--logfile", default=LOGFILE, type=str,
        help="Redirect logging to a file when daemonizing.")

    args = parser.parse_args(sys.argv[1:])

    # Grab everything we need from the manifest.
    manifest_dict = manifest.parse_manifest()

    if not "rdzinfo" in manifest_dict:
        sys.exit("No rdzinfo in the manifest")
        pass
    rdzinfo = manifest_dict["rdzinfo"]
    if not "element-id" in rdzinfo:
        sys.exit("No element-id in the manifest rdzinfo")
        pass
    element_id = rdzinfo["element-id"]
    if not "zmc-http" in rdzinfo:
        sys.exit("No zmc-http in the manifest rdzinfo")
        pass
    zmc_http = rdzinfo["zmc-http"]
    if not "element-token" in manifest_dict["passwords"]:
        sys.exit("No element-token in the manifest passwords")
        pass
    element_token = manifest_dict["passwords"]["element-token"]
    #
    # the zms user id is optional, if we do not get it, then we can assume
    # that the token is a privileged token or we got an admin token.
    #
    element_userid = None
    if "user-id" in rdzinfo:
        element_userid = rdzinfo["user-id"]
        pass
    elif "admin-token" in manifest_dict["passwords"]:
        element_token = manifest_dict["passwords"]["admin-token"]
        pass

    #
    # Grants are in the spectrum section. For now just one grant.
    #
    if not "spectrum" in manifest_dict:
        sys.exit("No spectrum in the manifest")
        pass
    if len(manifest_dict["spectrum"].keys()) == 0:
        sys.exit("No spectrum in the manifest")
        pass
    grant_keys = list(manifest_dict["spectrum"].keys())
    grant_id = grant_keys[0]

    global LOG
    LOG = logging.getLogger(__name__)    
    if args.debug:
        LOG.setLevel(logging.DEBUG)
        logging.getLogger('zmsclient').setLevel(logging.DEBUG)
    else:
        LOG.setLevel(logging.INFO)
        logging.getLogger('zmsclient').setLevel(logging.INFO)
    if args.debug > 1:
        logging.getLogger().setLevel(logging.DEBUG)
        pass

    ZMC = ZmsZmcClient(zmc_http, element_token,
                       detailed=False, raise_on_unexpected_status=True)
    
    # Then subscribe to the events
    if element_userid:
        filter = EventFilter(
            element_ids=[element_id], user_ids=[element_userid])
    else:
        filter = EventFilter(
            element_ids=[element_id])
        pass
    ZMCsubscription = ZMCSubscriptionCallback(
        ZMC, element_id,
        subscription=Subscription(
            id=str(uuid.uuid4()), filters=[filter]),
        grant_id=grant_id,
        impotent=args.impotent, reconnect_on_error=True)

    return ZMCsubscription, args.daemon, args.logfile

async def async_main(ZMCsubscription):
    this_task = asyncio.current_task();
    set_signal_handler(signal.SIGINT, this_task)
    set_signal_handler(signal.SIGHUP, this_task)
    set_signal_handler(signal.SIGTERM, this_task)

    try:
        await ZMCsubscription.start()
    except asyncio.CancelledError:
        ZMCsubscription.stop()
        pass
    pass

#
# The reason for this split of main, is cause we need to do the daemon
# fork() before heading into the asyncio main.
#
def main():
    ZMCsubscription, daemonize, logfile = init_main()
    format = "%(levelname)s:%(asctime)s:%(message)s"

    if daemonize:
        try:
            fp = open(logfile, "a");
            sys.stdout = fp
            sys.stderr = fp
            sys.stdin.close();
            logging.basicConfig(stream=fp, format=format)
            pass
        except:
            print("Could not open log file for append")
            sys.exit(1);
            pass
        pid = os.fork()
        if pid:
            sys.exit(0)
        os.setsid();
    else:
        logging.basicConfig(format=format)

    asyncio.run(async_main(ZMCsubscription))


if __name__ == "__main__":
    main()
