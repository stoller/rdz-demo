"""RDZ Demo Ware
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import IG extensions
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Spectrum specific extensions.
import geni.rspec.emulab.spectrum as spectrum
# Route specific extensions.
import geni.rspec.emulab.route as route
# RDZ extensions
import geni.rspec.emulab.rdz as rdz

IMAGEURN = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
PREBUILTURN = "urn:publicid:IDN+emulab.net+image+RDZ-demo//rdzdemo-profile"

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

def findName(list, key):
    for pair in list:
        if pair[0] == key:
            return pair[1]
        pass
    return None

# A list of endpoint sites.
endpoints = [
    ('', "None"),
    ('urn:publicid:IDN+bookstore.powderwireless.net+authority+cm',
     "Bookstore"),
    ('urn:publicid:IDN+cpg.powderwireless.net+authority+cm',
     "CPG"),
    ('urn:publicid:IDN+ebc.powderwireless.net+authority+cm',
     "EBC"),
    ('urn:publicid:IDN+guesthouse.powderwireless.net+authority+cm',
     "Guesthouse"),
    ('urn:publicid:IDN+humanities.powderwireless.net+authority+cm',
     "Humanities"),
    ('urn:publicid:IDN+law73.powderwireless.net+authority+cm',
     "Law"),
    ('urn:publicid:IDN+madsen.powderwireless.net+authority+cm',
     "Madsen"),
    ('urn:publicid:IDN+moran.powderwireless.net+authority+cm',
     "Moran"),
    ('urn:publicid:IDN+sagepoint.powderwireless.net+authority+cm',
     "Sagepoint"),
    ('urn:publicid:IDN+web.powderwireless.net+authority+cm',
     "WEB")]

# A list of radios on the Motherwhip.
radios = [
    ('', "None"),
    ('cbrssdr1-bes', 'cbrssdr1-bes'),
    ('cbrssdr1-browning','cbrssdr1-browning'),
    ('cbrssdr1-dentistry','cbrssdr1-dentistry'),
    ('cbrssdr1-fm','cbrssdr1-fm'),
    ('cbrssdr1-honors','cbrssdr1-honors'),
    ('cbrssdr1-hospital','cbrssdr1-hospital'),
    ('cbrssdr1-meb','cbrssdr1-meb'),
    ('cbrssdr1-smt','cbrssdr1-smt'),
    ('cbrssdr1-ustar','cbrssdr1-ustar'),
]

# Request a BS radio (with associated compute node).
pc.defineParameter("Radios", "Radio",
                   portal.ParameterType.STRING, [radios[0][0]], radios,
                   min=1, multiValue=1, itemDefaultValue='')

# Request a FE,
pc.defineParameter("Endpoints", "Fixed Endpoint",
                   portal.ParameterType.STRING, [endpoints[0][0]], endpoints,
                   min=1, multiValue=1, itemDefaultValue='')

pc.defineParameter("FreqLow", "Frequency Min",
                   portal.ParameterType.BANDWIDTH, 3360,
                   longDescription="This range is added to the radios. " +
                   "Values are rounded to the nearest kilohertz.")

pc.defineParameter("FreqHigh", "Frequency Max",
                   portal.ParameterType.BANDWIDTH, 3365,
                   longDescription="This range is added to the radios. " +
                   "Values are rounded to the nearest kilohertz.")

pc.defineParameter("FreqWidth", "Frequency Width",
                   portal.ParameterType.BANDWIDTH, 0,
                   longDescription="This is an RDZ specific option.")

pc.defineParameter("Power", "Power (dBm)",
                   portal.ParameterType.BANDWIDTH, -70,
                   longDescription="Power level in dBm.")

pc.defineParameter("violateTest",  "Run the RDZ violation test",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Run the RDZ TX violation test.")

pc.defineParameter("reallocTest",  "Run the RDZ grant reallocation test",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Run the RDZ grant reallocation test.")

pc.defineParameter("prebuilt",  "Use prebuilt image",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Use a prebuilt image.")

pc.defineParameter("heartBeats",  "Enable heartbeats",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Tell the RDZ to use/require heartbeats")

pc.defineParameter("innerRDZ","Inner RDZ",
                   portal.ParameterType.STRING, "",
                   longDescription="Optional name of the RDZinRDZ experiment "+
                   "to use for this experiment (pid,eid)")

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

if params.violateTest and params.reallocTest:
    if params.reallocTest:
        pc.reportError(portal.ParameterError(
            "You have also selected the violation test.", ["violateTest"]))
    else:
        pc.reportError(portal.ParameterError(
            "You have also selected the reallocation test.", ["reallocTest"]))
        pass
    pass

# Throw the errors back to the user.
pc.verifyParameters()

# Flag as needing the RDZ to allocate spectrum. Inner RDZ is optional
request.useRDZ(instance=params.innerRDZ);

#
# Radios that require an associate compute node.
#
for radioname in params.Radios:
    if radioname != "":
        radio = request.RawPC(radioname)
        radio.component_id = radioname
        radio.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
        radioiface = radio.addInterface();

        # Always need a compute node with a link to the radio.
        com = request.RawPC("com-" + radioname)
        com.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
        if params.prebuilt:
            com.disk_image = PREBUILTURN
        else:
            com.disk_image = IMAGEURN
            pass
        com.hardware_type = "d740"
        comiface = com.addInterface();
        com.addService(pg.Execute(
            shell="sh", command="sudo /bin/bash /local/repository/install.sh"))
        com.addService(pg.Execute(
            shell="sh", command="sudo /bin/bash /local/repository/install-zmsclient.sh"))

        com.startVNC()
        if params.violateTest and params.FreqHigh and params.FreqLow:
            com.addService(pg.Execute(
                shell="sh", command="/local/repository/rdztest.pl " + str(params.FreqLow)))
            pass
        elif params.reallocTest:
            com.addService(pg.Execute(
                shell="sh", command="/local/repository/txwrapper.pl --daemon"))
            pass

        if (radioname.startswith("cbrs") or radioname.startswith("cell") or
            radioname.startswith("ota") or radioname.startswith("oai")):
            radioiface.addAddress(pg.IPv4Address("192.168.40.2", "255.255.255.0"))
            comiface.addAddress(pg.IPv4Address("192.168.40.1", "255.255.255.0"))
        else:
            radioiface.addAddress(pg.IPv4Address("192.168.10.2", "255.255.255.0"))
            comiface.addAddress(pg.IPv4Address("192.168.10.1", "255.255.255.0"))
            pass
        link = request.Link(members = [radioiface, comiface])
        pass
    pass

#
# Add Fixed Endpoints
#
for endpoint in params.Endpoints:
    if endpoint != "":
        # Add a raw PC to the request
        node = request.RawPC(findName(endpoints, endpoint) + "-" + "nuc2")
        node.component_id = "nuc2"
        node.component_manager_id = endpoint
        if params.prebuilt:
            node.disk_image = PREBUILTURN
        else:
            node.disk_image = IMAGEURN
            pass
        node.startVNC()
        node.addService(pg.Execute(
            shell="sh", command="sudo /bin/bash /local/repository/install.sh"))
        node.addService(pg.Execute(
            shell="sh", command="sudo /bin/bash /local/repository/install-zmsclient.sh"))

        if params.violateTest and params.FreqHigh and params.FreqLow:
            node.addService(pg.Execute(
                shell="sh", command="/local/repository/rdztest.pl -g 55 " + str(params.FreqLow)))
            pass
        elif params.reallocTest:
            node.addService(pg.Execute(
                shell="sh", command="/local/repository/txwrapper.py --daemon"))
            pass
        pass
    pass

if params.FreqLow and params.FreqHigh:
    request.requestSpectrum(params.FreqLow, params.FreqHigh,
                            params.Power, params.FreqWidth)
    pass

if params.heartBeats:
    request.enableHeartbeats()
    pass

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
